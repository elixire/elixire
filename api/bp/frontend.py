# elixire: Image Host software
# Copyright 2018-2019, elixi.re Team and the elixire contributors
# SPDX-License-Identifier: AGPL-3.0-only

from pathlib import Path
from quart import Blueprint, send_file, current_app as app

bp = Blueprint("frontend", __name__)

FRONTEND_FOLDER = (Path.cwd() / "frontend" / "output").resolve()
ADMIN_PANEL_FOLDER = (Path.cwd() / "admin-panel" / "build").resolve()


async def maybe_send(base_folder: Path, user_path: str):
    # quart does ".." normalization for us, but it doesn't hurt to double check.
    # we do it both via checking user_path and by resolving the path to a real file
    # and seeing if the base_folder is in the path.
    if ".." in user_path:
        return "file not found", 404

    resolved_path = (base_folder / user_path).resolve(strict=True)

    if not str(resolved_path).startswith(str(base_folder)):
        return "file not found", 404

    if app.econfig.ENABLE_FRONTEND:
        return await send_file(resolved_path)

    return "frontend is not enabled"


@bp.route("/<path:path>")
async def frontend_path(path):
    """Map requests from / to /static."""
    return await maybe_send(FRONTEND_FOLDER, path)


@bp.get("/")
async def frontend_index():
    """Handler for the index page."""
    return await maybe_send(FRONTEND_FOLDER / "index.html", ".")


@bp.get("/admin/<path:path>")
async def admin_path(path):
    return await maybe_send(ADMIN_PANEL_FOLDER, path)


@bp.get("/admin")
async def admin_index():
    return await maybe_send(ADMIN_PANEL_FOLDER / "index.html", ".")


@bp.get("/robots.txt")
async def robots_txt():
    return await send_file("./static/robots.txt")


@bp.get("/humans.txt")
async def humans_txt():
    return await send_file("./static/humans.txt")
